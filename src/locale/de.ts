export default {
    back: 'Zurück',
    localeValue: 'De',
    help: 'Hilfe',
    disclaimer: 'Datenschutz',
    imprint: 'Impressum',
    contact: 'Kontakt',
    otherLocale: 'En',

    inputHeadline: 'Eingabe:',
    formHeadline: 'Formular:',
    metadataHeadline: 'Metadaten:',

    applicationProfileLabel: 'Application Profile',
    applicationProfileIdLabel: 'Shape Url',
    mimeTypeLabel: 'MimeType',
    selectMimeType: 'Bitte wählen Sie einen MimeType aus',
};
