export default {
    back: 'Back',
    localeValue: 'En',
    help: 'Help',
    disclaimer: 'Disclaimer',
    imprint: 'Imprint',
    contact: 'Contact',
    otherLocale: 'De',

    inputHeadline: 'Input:',
    formHeadline: 'Form:',
    metadataHeadline: 'Metadata:',

    applicationProfileLabel: 'Application Profile',
    applicationProfileIdLabel: 'Shape Url',
    mimeTypeLabel: 'MimeType',
    selectMimeType: 'Please select a mime type',
};
